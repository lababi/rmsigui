# ***********************************************************************************
# Module UI - Export gcode
# ***********************************************************************************
#' @title ExportGCodeInput
#' @param id identifier
#' @import utils
#' @importFrom stats setNames
#' @export ExportGCodeInput
ExportGCodeInput <- function( id ) {

  nameSpace <- shiny::NS(id)

  shiny::tagList(

    shiny::sidebarLayout(

      shiny::sidebarPanel(
        WFolderNameInput( nameSpace("ExportDir"), "Output directory" ),
        shiny::fluidRow(
          shiny::column( 4,
            shiny::numericInput(
              nameSpace("Width"),
              label = "width [mm]",
              value = 10,
              min   = 1,
              max   = 200,
              step  = 1  ) ),
          shiny::column( 4,
            shiny::numericInput(
              nameSpace("Height"),
              label = "height [mm]",
              value = 10,
              min   = 1,
              max   = 200,
              step  = 1  ) ),
          shiny::column( 4,
            shiny::numericInput(
              nameSpace("Step"),
              label = "step [mm]",
              value = 1,
              min   = 0.1,
              max   = 50,
              step  = 1  ) )
        ),
        shiny::fluidRow(
          shiny::column( 4,
            shiny::numericInput(
              nameSpace("Delay"),
              label = "wait [s]",
              value = 1,
              min   = 0.1,
              max   = 10,
              step  = 1  ) ),
          shiny::column( 4,
            style = "margin-top: 25px;",
            shiny::actionButton(
              nameSpace("SaveGCode"),
              label = " Gcode",
              icon  = shiny::icon("download"),
              width = "100%" ) ),
          shiny::column( 4,
            style = "margin-top: 25px;",
            shiny::actionButton(
              nameSpace("SaveCSV"),
              label = " CSV",
              icon  = shiny::icon("download"),
              width = "100%" ) )
        )
      ),

      shiny::mainPanel(
        HTML(
          paste(
            h3("Create RepRap G code and x,y coordinate file"),'<br/><br/>',
            "Please make sure that the R/shiny server has writing permission for the 
            ouput directory and verify that the files have been written correctly.",'<br/>',
            "The waiting time defines how long the robot will stop on each sampling point."
          )
        ),
        shiny::plotOutput( nameSpace("Grid") ) ,
        shiny::textOutput( nameSpace("Samples") ),
        shiny::textOutput( nameSpace("Time") )
      )
    )
  )
}


# ***********************************************************************************
# Module Server - Export gcode
# ***********************************************************************************
#' @title ExportGCode
#' @param input parameters of the imaging run, such as image dimensions and waiting time
#' @param output estimation of the sampling run with the given parameters
#' @param session shiny session
#' @export ExportGCode
ExportGCode <- function( input, output, session ) {

  # Get mzml source directory
  TargetDir <- callModule( WFolderName, "ExportDir" )

  # Render Gcode commands
  Gcode <- reactive({
    .gcode( input$Width, input$Height, input$Step, input$Delay )
  })

  # Display approximated acquisition time
  output$Samples <- renderText({
    paste(
      "Approximate sampling time: ",
      sprintf( "%.2f s", length( Gcode() ) * input$Delay )  )
  })

  # Display total sampling points
  output$Time <- renderText({
    paste(
      "Samplig spots/spectra: ",
      sprintf( "%.0f", length( Gcode() ) ) )
  })

  # Display sample distribution
  output$Grid <- renderPlot({

    coords <- .parsegcode( Gcode() )
    step   <- input$Step
    maxX   <- max( coords[,1] )
    maxY   <- max( coords[,2] )

    # Draw frame
    plot(
      c(-step, maxX+step, maxX+step, -step, -step),
      c(-step, -step, maxY+step, maxY+step, -step ),
      type  = "l",
      col   = "gray",
      frame = FALSE,
      xlab  = "",
      ylab  = "",
      asp   = 1,
      yaxt  = "n",
      xaxt  = "n" )

    # Graw grid
    points( coords[,1], coords[,2] )
  })

  # Save Gcode
  observeEvent( input$SaveGCode, {

    if( is.null( TargetDir() ) == TRUE )
      return( NULL )

    # Render output file name
    fileName <-
      paste0(
        TargetDir(), "/",
        "X_", input$Width,
        "-Y_", input$Height,
        "-RES_", input$Step,
        "-WAIT_", input$Delay,
        ".gcode" )

    # Save object - See also http://reprap.org/wiki/G-code#G-commands
    writeLines( c("G21", "G91", head(as.vector( Gcode() ), -1L)), con=fileName )
  })

  # Save CSV file
  observeEvent( input$SaveCSV, {

    if( is.null( TargetDir() ) == TRUE )
      return( NULL )

    # Render output file name
    fileName <-
      paste0(
        TargetDir(), "/",
        "X_", input$Width,
        "-Y_", input$Height,
        "-RES_", input$Step,
        "-WAIT_", input$Delay,
        ".csv" )

    # Render CSV content
    nx <- trunc( input$Width / input$Step )
    ny <- trunc( input$Height / input$Step )
    xy <-
      cbind(
        x = rep( c(0L:nx, nx:0L), length.out = (nx + 1L) * (ny + 1L) ),
        y = rep( 0L:ny, each=nx + 1L) )

    # Write CSV
    write.table( xy, file=fileName, sep=",", row.names=FALSE, col.names=FALSE )
  })
}


# ***********************************************************************************
#' Creates a gcode content (workhorse of gcode)
#' @param x sampling area in x direction in mm
#' @param y sampling area in y direction in mm
#' @param resolution in mm
#' @param wait waiting time in sec
#' @seealso http://reprap.org/wiki/G-code#G-commands
#' @noRd
# ***********************************************************************************
.gcode <- function( x, y, resolution, wait ) {

  # Calculate cell number on each axis
  nx <- trunc(x/resolution) + 1L
  ny <- trunc(y/resolution) + 1L

  # Matrix with alternate TRUE on odd rows and FALSE on even rows
  move <- logical(nx * ny)
  move[1L:nx + nx * rep(seq(0L, ny - 1L, by=2), each=nx)] <- TRUE

  # Render gcode: delay & move on x axis
  m <-
  matrix(
    c( rep( sprintf("G4 S%f", wait), length( move ) ),
       sprintf(ifelse(move, "G0 X%f", "G0 X-%f"), resolution) ),
    nrow  = 2,
    byrow = TRUE )

  # Render gcode: move on y axis
  m[ 2L, seq(nx, ncol(m), by=nx) ] <- sprintf("G0 Y%f", resolution)
  return( m )
}


# ***********************************************************************************
#' Parses gcode content (second row of .gcode output matrix)
#' @param x character vector
#' @seealso http://reprap.org/wiki/G-code#G-commands
#' @noRd
# ***********************************************************************************
.parsegcode <- function( x, relative=FALSE ) {

  # Get cells with move commands
  x <- x[grepl("^G0", x)]

  # gcode to xyz coordinates
  pos <- lapply(
    strsplit(x, "\\s+"),
    function(xx) {
      xx <- xx[-1L]
      xx <- setNames(as.double(substr(xx, 2L, nchar(xx))), tolower(substr(xx, 1L, 1L)))
      as.numeric( xx[c("x", "y", "z")] )
    } )

  n   <- length( pos )
  pos <- do.call(rbind, c( list(0), pos ) )

  colnames(pos)     <- c("x", "y", "z")
  pos[ is.na(pos) ] <- 0L

  if( !relative )
    pos <- apply( pos, 2L, cumsum )

  return( pos[ 1:n, ] )
}
