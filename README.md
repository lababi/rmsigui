# README #

RmsiGUI is a graphical user interface for analyzing mass spectrometry imaging (MSI) data with the statistical language R <https://www.r-project.org/>. 

### Command line scrips ###

You can find the MSI.R command line scripts at https://bitbucket.org/lababi/msi.r/.

### System requirements ###

- Please install the 64-bit versions of Java and R. Please de-install the 32-bit versions, since they cause problems in installing/running RmsiGUI.
- libcairo is required on Linux. Installation on Debian/ Ubuntu: sudo apt-get install libcairo-dev
- R package `rJava`: install.packages('rJava'). In case of problems on Debian/ Ubuntu, try: `sudo apt update && sudo apt install default-jdk && sudo R CMD javareconf`. Then the R package 'rJava' should be installable.

### Installing the R package from bitbucket (current development version) ###

1. Install devtools:    
``install.packages('devtools')``
2. Install R package from BitBucket:  
``library(devtools)``  
``install_bitbucket('lababi/rmsigui')``    
3. Run RmsiGUI by:  
``library(RmsiGUI)``  
``launch()``  

If everything goes well, you get a message with the IP of the GUI:  
``Listening on http://127.0.0.1:4353``  

Now you can open the RmsiGUI interface in a web browser.

### Installing/running from the downloaded R project ###

1. Clone the repository using git (<https://git-scm.com/>) by:  
`` git clone https://lababi@bitbucket.org/lababi/rmsigui.git`` 
2. Open the project file RmsiGUI.Rproj with RStudio (<https://www.rstudio.com/>)  
3. In the 'Build' tab, choose 'Install and Restart'
4. Run RmsiGUI by:  
``library(RmsiGUI)``  
``launch()``  
5. Open the provided **RmsiGUI IP** in your favorite browser.  

Alternately, by using the tar.gz file you can do the next steps:
1. run in the command line:
``install.packages("path_to_rmsiGUI_0.1.0_R_.tar.gz", repos = NULL, type = "source")``
``library(RmsiGUI)``  
``launch()`` 

### Install the Windows binary R package ###

For Windows, also a binary package is available (please write me an email for the newest release):  
1. Download the RmsiGUI_x.x.x.zip  
2. Install by ``R CMD INSTALL RmsiGUI_x.x.x.zip`` or using RStudio ('Tools')  
3. Run RmsiGUI by:  
``library(RmsiGUI)``  
``launch()``  
4. Open the provided **RmsiGUI IP** in your favorite browser.  

### reading imzML data ###

imzML and ibd files have to be in the same directory.

### Contribution guidelines ###

Please report your experiences, feature requests and problems.

### Reference ###

If RmsiGUI is useful for you, please cite:
Ignacio Rosas-Román, Cesarè Ovando-Vázquez, Abigail Moreno-Pedraza, Hèctor Guillén-Alonso, Robert Winkler (2020),
Open LabBot and RmsiGUI: Community development kit for sampling automation and ambient imaging,
Microchemical Journal, Volume 152, 104343, <https://doi.org/10.1016/j.microc.2019.104343>

### License ###

GNU General Public License, version 3 (http://gplv3.fsf.org/).

### Contact ###

robert.winkler@cinvestav.mx, robert.winkler.mail@gmail.com